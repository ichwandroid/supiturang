<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
  .tabel {
    width: 100%;
    height: 90px;
    text-align: center;
  }

  .tabel td {
    border: solid 1px;
  }

  .head {
    background-color: #87CB16;

  }
</style>

<div class="content">
  <div class="container-fluid">
    <div class="row col-md-12">
      <div class="header">
        <h4 class="title">Waktu Pembuangan</h4>
      </div>

      <!-- form -->
      <form method="post" action="pasar_waktupembuangan.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>No. Polisi</label>
            <input type="text" name="nopol" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            &nbsp;
          </div>
        </div>
      </form>

        <?php

        if (isset($_POST['pencarian'])) {
          
          $nopol  = $_POST['nopol'];          

          if (empty($nopol)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'pasar_waktupembuangan.php';
          </script>
          
          <?php
          } else {
          ?>

          <div class="col-md-12">            
            <p>Informasi hasil pencarian No. Polisi <?php echo $_POST['nopol'];?></p>
          </div>

          <?php
            $query = mysql_query("SELECT in_out_pasar.id, in_out_pasar.tanggal, 
              ((in_out_pasar.vol1 LIKE 'A')+(in_out_pasar.vol2 LIKE 'A')+(in_out_pasar.vol3 LIKE 'A')+(in_out_pasar.vol4 LIKE 'A')+(in_out_pasar.vol5 LIKE 'A')+(in_out_pasar.vol6 LIKE 'A')+(in_out_pasar.vol7 LIKE 'A')+(in_out_pasar.vol1 LIKE 'B')+(in_out_pasar.vol2 LIKE 'B')+(in_out_pasar.vol3 LIKE 'B')+(in_out_pasar.vol4 LIKE 'B')+(in_out_pasar.vol5 LIKE 'B')+(in_out_pasar.vol6 LIKE 'B')+(in_out_pasar.vol7 LIKE 'B')+(in_out_pasar.vol1 LIKE 'C')+(in_out_pasar.vol2 LIKE 'C')+(in_out_pasar.vol3 LIKE 'C')+(in_out_pasar.vol4 LIKE 'C')+(in_out_pasar.vol5 LIKE 'C')+(in_out_pasar.vol6 LIKE 'C')+(in_out_pasar.vol7 LIKE 'C')) AS 'total_ritasi', 
              timediff(in_out_pasar.keluar1,in_out_pasar.masuk1) AS 'selisih1',
              timediff(in_out_pasar.keluar2,in_out_pasar.masuk2) AS 'selisih2',
              timediff(in_out_pasar.keluar3,in_out_pasar.masuk3) AS 'selisih3',
              timediff(in_out_pasar.keluar4,in_out_pasar.masuk4) AS 'selisih4',
              timediff(in_out_pasar.keluar5,in_out_pasar.masuk5) AS 'selisih5',
              timediff(in_out_pasar.keluar6,in_out_pasar.masuk6) AS 'selisih6',
              timediff(in_out_pasar.keluar7,in_out_pasar.masuk7) AS 'selisih7',
              ((in_out_pasar.keluar1 - in_out_pasar.masuk1 + in_out_pasar.keluar2 - in_out_pasar.masuk2 + in_out_pasar.keluar3 - in_out_pasar.masuk3 + in_out_pasar.keluar4 - in_out_pasar.masuk4 + in_out_pasar.keluar5 - in_out_pasar.masuk5 + in_out_pasar.keluar6 - in_out_pasar.masuk6 + in_out_pasar.keluar7 - in_out_pasar.masuk7) / ((in_out_pasar.vol1 LIKE 'A')+(in_out_pasar.vol2 LIKE 'A')+(in_out_pasar.vol3 LIKE 'A')+(in_out_pasar.vol4 LIKE 'A')+(in_out_pasar.vol5 LIKE 'A')+(in_out_pasar.vol6 LIKE 'A')+(in_out_pasar.vol7 LIKE 'A')+(in_out_pasar.vol1 LIKE 'B')+(in_out_pasar.vol2 LIKE 'B')+(in_out_pasar.vol3 LIKE 'B')+(in_out_pasar.vol4 LIKE 'B')+(in_out_pasar.vol5 LIKE 'B')+(in_out_pasar.vol6 LIKE 'B')+(in_out_pasar.vol7 LIKE 'B')+(in_out_pasar.vol1 LIKE 'C')+(in_out_pasar.vol2 LIKE 'C')+(in_out_pasar.vol3 LIKE 'C')+(in_out_pasar.vol4 LIKE 'C')+(in_out_pasar.vol5 LIKE 'C')+(in_out_pasar.vol6 LIKE 'C')+(in_out_pasar.vol7 LIKE 'C'))) AS 'waktu_rata'
              FROM in_out_pasar INNER JOIN kendaraan ON in_out_pasar.id_kendaraan = kendaraan.id_kendaraan WHERE nopol LIKE '$nopol'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width">
              <table class="tabel">
                <tr class="head">
                  <td>No.</td>
                  <td>Tanggal</td>
                  <td>Total Selisih Waktu keluar Masuk</td>
                  <td>Jumlah Ritasi</td>
                  <td>Rata - Rata Waktu Yang Diperlukan</td>
                </tr>

              <?php
              $totaljumlahritasi = 0;
              $totalrata = 0;
              $nomor = 1;
              while ($data = mysql_fetch_array($query)) {
              ?>

                <tbody>
                  <tr>
                    <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['tanggal']; ?></td>
                      <?php 
                    $total_selisih = [];
                    
                      for($ich=1; $ich<=7;$ich++){
                        $times[] = $data["selisih$ich"];
                       }

                      $ts = sum_time($times);                      
                    ?>
                    <td><?php echo $ts;?></td>
                    <td><?php echo $data['total_ritasi'];?></td>
                    <td><?php $rata_ritasi = avg_time($ts,$data['total_ritasi']); echo $rata_ritasi;?></td>
                  </tr>
                </tbody>

                <?php $totaljumlahritasi += $data['total_ritasi']; ?>

                <?php 
                  $h1 = []; // function calc_time1
                  $kolom1[] = avg_time($ts,$data['total_ritasi']);
                  $totalrata = sum_time1($kolom1);
                ?>
              
              <?php } 
                    if (mysql_num_rows($query)==0) {
                      echo "<p>pencarian tidak ditemukan</p>";
                    }
                  ?>
                  <tfoot class="head">
                    <tr>
                      <td colspan="3">Total</td>
                      <td><?php echo $totaljumlahritasi;?></td>
                      <td><?php echo $totalrata;?></td>
                    </tr>
                    <tr>
                      <td colspan="4">Rata - Rata Per Bulan</td>
                      <td><?php $tot = $totalrata/$totaljumlahritasi; echo number_format($tot,2);?></td>
                    </tr>
                  </tfoot>
            </table>
        </div>
        <br>
        <div class="col-md-12">
          <form method="post" action="pasar/pasar_waktupembuangan.php" target="_blank">
            <div class="col-md-10">
              <div class="form-group" hidden="hidden">
                <label>No. Polisi</label>
                <input type="text" value="<?php echo $_POST['nopol'];?>" name="nopol" class="form-control">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
              </div>
            </div>
          </form>
        </div>

      <?php
        } else { unset($_POST['pencarian']); }  
      ?>           

    </div>
</div>

<?php include('template/footer.php');?>