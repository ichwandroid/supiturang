<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
      .tabel {
            width: 100%;
            text-align: center;
      }

      .tabel td {
            padding: 10px;
            border: solid 1px #000;
      }

      .head {
            background-color: #87CB16;
            height: 40px;
            border: solid 1px #000;
      }

      .head td {
            border: solid 1px #000;
      }

      .head1 td {
            width: 50%;
            padding: 10px;
            background-color: #87CB16;
            border: solid 1px #000;
      }
</style>

<div class="content">
  <div class="container-fluid">
    <div class="row col-md-12">
      <div class="header">
        <h4 class="title">Masukkan tanggal yang dicari</h4>
      </div>

      <!-- form -->
      <form method="post" action="dlh_pembuangan.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d', strtotime('-31 days', strtotime(date('Y-m-d'))));?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d');?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
      </form>

      <?php

        if (isset($_POST['pencarian'])) {
          
          $tanggalawal  = $_POST['tanggalawal'];
          $tanggalakhir = $_POST['tanggalakhir'];

          if (empty($tanggalawal)||empty($tanggalakhir)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'dlh_pembuangan.php';
          </script>
          
          <?php
          } else {
          ?>

          <p>Informasi hasil pencarian tanggal <?php echo $_POST['tanggalawal'];?> sampai tanggal <?php echo $_POST['tanggalakhir'];?></p>

          <?php
            $query = mysql_query("SELECT in_out_dlh.id, kendaraan.jenis, kendaraan.nopol, kendaraan.lamb, kendaraan.sopir,
              ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')+(in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')+(in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C')) AS 'total_ritasi', 
              timediff(in_out_dlh.masuk2,in_out_dlh.masuk1) AS 'selmasuk1', 
              timediff(in_out_dlh.masuk3,in_out_dlh.masuk2) AS 'selmasuk2', 
              timediff(in_out_dlh.masuk4,in_out_dlh.masuk3) AS 'selmasuk3', 
              timediff(in_out_dlh.masuk5,in_out_dlh.masuk4) AS 'selmasuk4',
            ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')) AS 'volA',
            ((in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')) AS 'volB',
            ((in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C')) AS 'volC',
            timediff(in_out_dlh.keluar1,in_out_dlh.masuk1) AS 'selisih1',
            timediff(in_out_dlh.keluar2,in_out_dlh.masuk2) AS 'selisih2',
            timediff(in_out_dlh.keluar3,in_out_dlh.masuk3) AS 'selisih3',
            timediff(in_out_dlh.keluar4,in_out_dlh.masuk4) AS 'selisih4',
            timediff(in_out_dlh.keluar5,in_out_dlh.masuk5) AS 'selisih5',
            timediff(in_out_dlh.keluar6,in_out_dlh.masuk6) AS 'selisih6',
            timediff(in_out_dlh.keluar7,in_out_dlh.masuk7) AS 'selisih7',
            time((in_out_dlh.keluar1 - in_out_dlh.masuk1 + in_out_dlh.keluar2 - in_out_dlh.masuk2 + in_out_dlh.keluar3 - in_out_dlh.masuk3 + in_out_dlh.keluar4 - in_out_dlh.masuk4 + in_out_dlh.keluar5 - in_out_dlh.masuk5 + in_out_dlh.keluar6 - in_out_dlh.masuk6 + in_out_dlh.keluar7 - in_out_dlh.masuk7) / ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')+(in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')+(in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C'))) AS 'waktu_rata' 
            FROM in_out_dlh INNER JOIN kendaraan ON in_out_dlh.id_kendaraan = kendaraan.id_kendaraan WHERE tanggal BETWEEN '$tanggalawal' AND '$tanggalakhir'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width frame">
            <table class="tabel">
              <tr class="head">
                <td rowspan="2">No.</td>
                <td rowspan="2">Jenis Kendaraan</td>
                <td rowspan="2">No. Polisi</td>
                <td rowspan="2">No. Lambung</td>
                <td rowspan="2">Nama Supir</td>
                <td colspan="3">Jumlah Ritasi</td>
                <td rowspan="2">Total Ritasi</td>
                <td colspan="3">Volume Sampah</td>
                <td rowspan="2">Total Volume Sampah</td>
                <td colspan="2">Waktu</td>
              </tr>
              <tr class="head1">
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>Rata - Rata Pembuangan di TPA</td>
                <td>Rata - Rata per RIT</td>
              </tr>

              <?php
              $a1 = 0;
              $nomor = 1;
              $volA = 0;
              $volB = 0;
              $volC = 0;
              $jumlahperrit = 0;
              $totalritasi = 0; 
              $totvolA = 0;
              $totvolB = 0;
              $totvolC = 0;
              $totalvolume = 0;

              while ($data = mysql_fetch_array($query)) {
              ?>

              <tbody>
                <tr>
                  <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['jenis']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>
                    <td><?php echo $data['lamb']; ?></td>
                    <td><?php echo $data['sopir']; ?></td>
                    <td><?php echo $data['volA']; ?></td>
                    <td><?php echo $data['volB']; ?></td>
                    <td><?php echo $data['volC']; ?></td>
                    <td><?php echo ($data['volA']+$data['volB']+$data['volC']);?></td>
                    <td><?php echo ($data['volA']*7); ?></td>
                    <td><?php echo ($data['volB']*9); ?></td>
                    <td><?php echo ($data['volC']*11); ?></td>
                    <td><?php $totalvol = ($data['volA']*7)+($data['volB']*9)+($data['volC']*11); echo $totalvol; ?></td>
                    <?php 
                      $total_selisih = [];
                        for($ich=1; $ich<=7;$ich++){
                          $times[] = $data["selisih$ich"];
                        }

                      $ts = sum_time($times);                      
                    ?>
                    <td><?php $rata_ritasi = avg_time($ts,$data['total_ritasi']); echo $rata_ritasi;?></td>

                    <?php 
                    // $total_selisih[] = [];
                      for($ih=1; $ih<=4;$ih++){
                        $a1 += strlen($data["selmasuk$ih"]>0) ? 1 :0;
                        $jumlahperrit = $a1;

                        if ($data["selmasuk$ih"] > 0) {
                          $total_selisih[] = $data["selmasuk$ih"]; 
                        } 
                      }
                    ?>
                    <td><?php $sum = sum_time($total_selisih); echo avg_time($sum, $a1);?></td>

                </tr>
              </tbody>

              <?php $totalritasi += ($data['volA']+$data['volB']+$data['volC']);?>
              <?php $totvolA += ($data['volA']*7);?>
              <?php $totvolB += ($data['volB']*9);?>
              <?php $totvolC += ($data['volC']*11);?>
              <?php $totalvolume += $totalvol;?>
              <?php
                $h6 = [];
                $kolomratarata[] = avg_time($ts,$data['total_ritasi']);
                $totalratarata = sum_time6($kolomratarata);
              ?>
              <?php
                $h7 = [];
                $kolomrataratarit[] = avg_time($sum, $a1);
                $totalrataratarit = sum_time7($kolomrataratarit);
              ?>
              
              <?php } 
                    if (mysql_num_rows($query)==0) {
                      echo "<p>pencarian tidak ditemukan</p>";
                    }
                  ?>
              <tfoot class="head">
                <tr>
                  <td colspan="8">Total</td>
                  <td><?php echo $totalritasi;?></td>
                  <td><?php echo $totvolA;?></td>
                  <td><?php echo $totvolB;?></td>
                  <td><?php echo $totvolC;?></td>
                  <td><?php echo $totalvolume;?></td>
                  <td><?php echo $totalratarata;?></td>
                  <td><?php echo $totalrataratarit;?></td>
                </tr>
                <tr>
                  <td colspan="8">Rata - Rata Per Hari</td>
                  <td><?php echo number_format($totalritasi/28,2);?></td>
                  <td><?php echo number_format($totvolA/28,2);?></td>
                  <td><?php echo number_format($totvolB/28,2);?></td>
                  <td><?php echo number_format($totvolC/28,2);?></td>
                  <td><?php echo number_format($totalvolume/28,2);?></td>
                  <td><?php echo number_format($totalratarata/28,2);?></td>
                  <td><?php echo number_format($totalrataratarit/28,2);?></td>
                </tr>
                </tr>
              </tfoot>
            </table>


    </div>
    <br>
    <div class="col-md-12">
    <form method="post" action="dlh/dlh_pembuangan.php" target="_blank">
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Awal</label>
            <input type="date" value="<?php echo $_POST['tanggalawal'];?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>&nbsp;</label>
            <button type="submit" name="pencarian" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
          </div>
        </div>
      </form>
    </div>
      <?php
        } else { unset($_POST['pencarian']); }  
      ?>
  </div>
</div>

<?php include('template/footer.php');?>