<?php
function calc_time(array $total_selisih){
  $i = 0;
  foreach ($total_selisih as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time(array $times) {
  $minutes = 0;
      // loop throught all the times
      foreach ($times as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 

function avg_time($total, $count, $rounding = 0) {
  $total = explode(":", strval($total));
  if (count($total) !== 3) return false;
  $sum = $total[0]*60*60 + $total[1]*60 + $total[2];
  if ($sum == 0) {
    $average = 00;
  } else {
    $average = $sum/(float)$count;
  }
  $hours = floor($average/3600);
  $minutes = floor(fmod($average,3600)/60);
  $seconds = number_format(fmod(fmod($average,3600),60),(int)$rounding);
  return $hours.":".$minutes.":".$seconds;
}


// ------------------------------- Total waktu pembuangan --------------------------------------------------------

function calc_time1(array $h1){
  $i = 0;
  foreach ($h1 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time1(array $rata_ritasi) {
  $minutes = 0;
      // loop throught all the times
      foreach ($rata_ritasi as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 

// ------------------------------- Total waktu masuk --------------------------------------------------------

function calc_time2(array $h2){
  $i = 0;
  foreach ($h2 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time2(array $total_selisih) {
  $minutes = 0;
      // loop throught all the times
      foreach ($total_selisih as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
}

function calc_time3(array $h3){
  $i = 0;
  foreach ($h3 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time3(array $sum) {
  $minutes = 0;
      // loop throught all the times
      foreach ($sum as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 


// ----------------------- Total Waktu Laporan ------------------

function calc_time4(array $h4){
  $i = 0;
  foreach ($h4 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time4(array $total_selisih) {
  $minutes = 0;
      // loop throught all the times
      foreach ($total_selisih as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
}

function calc_time5(array $h5){
  $i = 0;
  foreach ($h5 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time5(array $sum) {
  $minutes = 0;
      // loop throught all the times
      foreach ($sum as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 


//--------------------- pembungan -----------------

function calc_time6(array $h6){
  $i = 0;
  foreach ($h6 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time6(array $rata_ritasi) {
  $minutes = 0;
      // loop throught all the times
      foreach ($rata_ritasi as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 

function calc_time7(array $h7){
  $i = 0;
  foreach ($h7 as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time7(array $sum) {
  $minutes = 0;
      // loop throught all the times
      foreach ($sum as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 

?>