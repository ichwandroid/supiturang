<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
      .tabel {
            width: 100%;
            text-align: center;
      }

      .tabel td {
            padding: 10px;
            border: solid 1px #000;
      }

      .head {
            background-color: #87CB16;
            height: 40px;
            border: solid 1px #000;
      }

      .head td {
            border: solid 1px #000;
      }

      .head1 td {
            width: 50%;
            padding: 10px;
            background-color: #87CB16;
            border: solid 1px #000;
      }
</style>

<div class="content">
	<div class="container-fluid">
		<div class="row col-md-12">
			<div class="header">
        <h4 class="title">Masukkan tanggal yang dicari</h4>
      </div>

      <!-- form -->
      <form method="post" action="dlh_laporan.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d', strtotime('-31 days', strtotime(date('Y-m-d'))));?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?= date('Y-m-d');?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
      </form>

      <?php

        if (isset($_POST['pencarian'])) {
          
          $tanggalawal  = $_POST['tanggalawal'];
          $tanggalakhir = $_POST['tanggalakhir'];

          if (empty($tanggalawal)||empty($tanggalakhir)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'dlh_laporan.php';
          </script>
          
          <?php
          } else {
          ?>

          <p>Informasi hasil pencarian tanggal <?php echo $_POST['tanggalawal'];?> sampai tanggal <?php echo $_POST['tanggalakhir'];?></p>
          
          <?php
            $query = mysql_query("SELECT in_out_dlh.id, in_out_dlh.tanggal, kendaraan.jenis, kendaraan.nopol, kendaraan.lamb, kendaraan.sopir, 
                    in_out_dlh.masuk1, in_out_dlh.keluar1, timediff(in_out_dlh.keluar1,in_out_dlh.masuk1) AS 'selisih1', 
                    in_out_dlh.masuk2, in_out_dlh.keluar2, timediff(in_out_dlh.keluar2,in_out_dlh.masuk2) AS 'selisih2', 
                    in_out_dlh.masuk3, in_out_dlh.keluar3, timediff(in_out_dlh.keluar3,in_out_dlh.masuk3) AS 'selisih3', 
                    in_out_dlh.masuk4, in_out_dlh.keluar4, timediff(in_out_dlh.keluar4,in_out_dlh.masuk4) AS 'selisih4', 
                    in_out_dlh.masuk5, in_out_dlh.keluar5, timediff(in_out_dlh.keluar5,in_out_dlh.masuk5) AS 'selisih5', 
                    in_out_dlh.masuk6, in_out_dlh.keluar6, timediff(in_out_dlh.keluar6,in_out_dlh.masuk6) AS 'selisih6', 
                    in_out_dlh.masuk7, in_out_dlh.keluar7, timediff(in_out_dlh.keluar7,in_out_dlh.masuk7) AS 'selisih7', 
                    ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')+(in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')+(in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C')) AS 'total_ritasi',
                    ((in_out_dlh.keluar1 - in_out_dlh.masuk1 + in_out_dlh.keluar2 - in_out_dlh.masuk2 + in_out_dlh.keluar3 - in_out_dlh.masuk3 + in_out_dlh.keluar4 - in_out_dlh.masuk4 + in_out_dlh.keluar5 - in_out_dlh.masuk5 + in_out_dlh.keluar6 - in_out_dlh.masuk6 + in_out_dlh.keluar7 - in_out_dlh.masuk7) / ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')+(in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')+(in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C'))) AS 'waktu_rata' FROM in_out_dlh INNER JOIN kendaraan ON in_out_dlh.id_kendaraan = kendaraan.id_kendaraan WHERE tanggal BETWEEN '$tanggalawal' AND '$tanggalakhir'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width frame">
            <table class="tabel">
              <tr class="head">
                <td rowspan="2">No.</td>
                <td rowspan="2">Jenis Kendaraan</td>
                <td rowspan="2">No. Polisi</td>
                <td rowspan="2">No. Lambung</td>
                <td rowspan="2">Nama Supir</td>
                <td colspan="3">Waktu 1</td>
                <td colspan="3">Waktu 2</td>
                <td colspan="3">Waktu 3</td>
                <td colspan="3">Waktu 4</td>
                <td colspan="3">Waktu 5</td>
                <td colspan="3">Waktu 6</td>
                <td colspan="3">Waktu 7</td>
                <td rowspan="2">Jumlah Ritasi</td>
                <td rowspan="2">Total Selisih Waktu keluar Masuk</td>
                <td rowspan="2">Rata - Rata Waktu Yang Diperlukan</td>
              </tr>
              <tr class="head1">
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
                <td>Masuk</td>
                <td>Keluar</td>
                <td>Selisih</td>
              </tr>

              <?php
              $nomor = 1;
              $total_ritasi = 0;
              while ($data = mysql_fetch_array($query)) {
                // $a1 = 0;
              ?>

              <tbody>
                <tr>
                  <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['jenis']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>
                    <td><?php echo $data['lamb']; ?></td>
                    <td><?php echo $data['sopir']; ?></td>
                    <td><?php echo $data['masuk1']; ?></td>
                    <td><?php echo $data['keluar1']; ?></td>
                    <td><?php echo $data['selisih1']; ?></td>
                    <td><?php echo $data['masuk2']; ?></td>
                    <td><?php echo $data['keluar2']; ?></td>
                    <td><?php echo $data['selisih2']; ?></td>
                    <td><?php echo $data['masuk3']; ?></td>
                    <td><?php echo $data['keluar3']; ?></td>
                    <td><?php echo $data['selisih3']; ?></td>
                    <td><?php echo $data['masuk4']; ?></td>
                    <td><?php echo $data['keluar4']; ?></td>
                    <td><?php echo $data['selisih4']; ?></td>
                    <td><?php echo $data['masuk5']; ?></td>
                    <td><?php echo $data['keluar5']; ?></td>
                    <td><?php echo $data['selisih5']; ?></td>
                    <td><?php echo $data['masuk6']; ?></td>
                    <td><?php echo $data['keluar6']; ?></td>
                    <td><?php echo $data['selisih6']; ?></td>
                    <td><?php echo $data['masuk7']; ?></td>
                    <td><?php echo $data['keluar7']; ?></td>
                    <td><?php echo $data['selisih7']; ?></td>
                    <td><?php $a1=$data['total_ritasi']; echo $data['total_ritasi']; ?></td>
                    <?php for ($ch=1; $ch <= 7 ; $ch++) { $total_selisih[] = $data["selisih$ch"]; } ?>          
                    <td><?php echo sum_time($total_selisih); ?></td>
                    <td><?php $sum = sum_time($total_selisih); echo avg_time($sum, $a1); ?>                      
                    </td>
                </tr>
              </tbody>
              
              <?php $total_ritasi += $a1; ?>
              
              <?php
               $h4 = [];
               $kolomtotalselisih[] = sum_time($total_selisih);
               $totalselisih = sum_time4($kolomtotalselisih);
              ?>

              <?php
                $h5 = [];
                $kolomratarata[] = avg_time($sum, $a1);
                $totalratarata = sum_time5($kolomratarata);
              ?>

              <?php } if (mysql_num_rows($query)==0) { echo "<p>pencarian tidak ditemukan</p>"; } ?>

              <tfoot class="head">
                <tr>
                  <td colspan="26">TOTAL RITASI & TOTAL RATA-RATA WAKTU YANG DIPERLUKAN </td>
                  <td><?php echo $total_ritasi; ?></td>
                  <td><?php echo $totalselisih; ?></td>
                  <td><?php echo $totalratarata; ?></td>
                </tr>
              </tfoot>
            </table>

    </div>
    <br>
    <div class="col-md-12">
    <form method="post" action="dlh/dlh_laporan.php" target="_blank">
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Awal</label>
            <input type="date" value="<?php echo $_POST['tanggalawal'];?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>&nbsp;</label>
            <button type="submit" name="pencarian" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
          </div>
        </div>
      </form>
    </div>
      <?php
        } else { unset($_POST['pencarian']); 
      }  
      ?>

  </div>
</div>

<?php include('template/footer.php');?>