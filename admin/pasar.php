<?php include ('template/header.php'); ?>

        <div class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="header">
                        <h4 class="title">&nbsp;</h4>
                        <p class="category">&nbsp;</p>

                    </div>
                    <div class="content">
                        <div class="places-buttons">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <h5>Laporan Bulanan
                                        <p class="category">Dinas Kebersihan dan Pertamanan Pasar</p>
                                    </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-md-offset-1">
                                    <a class="btn btn-success btn-block" href="pasar_pembuangan.php">Pembuangan<br> Sampah Bulanan</a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success btn-block" href="pasar_volume.php">Volume<br> Bulanan</a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success btn-block" href="pasar_waktumasuk.php">Waktu Masuk<br> Antar Ritasi</a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success btn-block" href="pasar_waktupembuangan.php">Waktu<br> Pembuangan</a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success btn-block" href="pasar_laporan.php">Laporan<br> Sampah Perhari</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include ('template/footer.php'); ?>