<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
  .tabel {
    width: 100%;
    height: 90px;
    text-align: center;
  }

  .tabel td {
    border: solid 1px;
  }

  .head {
    background-color: #87CB16;

  }
</style>

<div class="content">
  <div class="container-fluid">
    <div class="row col-md-12">
      <div class="header">
        <h4 class="title">Waktu Masuk Antar Ritasi</h4>
      </div>

      <!-- form -->
      <form method="post" action="perdagangan_waktumasuk.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>No. Polisi</label>
            <input type="text" name="nopol" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            &nbsp;
          </div>
        </div>
      </form>

        <?php

        if (isset($_POST['pencarian'])) {
          
          $nopol  = $_POST['nopol'];          

          if (empty($nopol)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'perdagangan_waktumasuk.php';
          </script>
          
          <?php
          } else {
          ?>

          <div class="col-md-12">            
            <p>Informasi hasil pencarian No. Polisi <?php echo $_POST['nopol'];?></p>
          </div>

          <?php
            $query = mysql_query("SELECT in_out_perdagangan.id, in_out_perdagangan.tanggal, kendaraan.nopol, in_out_perdagangan.masuk1, in_out_perdagangan.masuk2, in_out_perdagangan.masuk3, in_out_perdagangan.masuk4, in_out_perdagangan.masuk5,
              timediff(in_out_perdagangan.masuk2,in_out_perdagangan.masuk1) AS 'selisih1', 
              timediff(in_out_perdagangan.masuk3,in_out_perdagangan.masuk2) AS 'selisih2', 
              timediff(in_out_perdagangan.masuk4,in_out_perdagangan.masuk3) AS 'selisih3', 
              timediff(in_out_perdagangan.masuk5,in_out_perdagangan.masuk4) AS 'selisih4'
              FROM in_out_perdagangan INNER JOIN kendaraan ON in_out_perdagangan.id_kendaraan = kendaraan.id_kendaraan WHERE nopol LIKE '$nopol'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width">
              <table class="tabel">
                <tr class="head">
                  <td rowspan="2">No.</td>
                  <td rowspan="2">Tanggal</td>
                  <td rowspan="2">No. Polisi</td>
                  <td colspan="5">Waktu Masuk TPA</td>
                  <td colspan="4">Selisih Waktu per RIT</td>
                  <td rowspan="2">Jumlah per RIT</td>
                  <td rowspan="2">Total Selisih Ritasi</td>
                  <td rowspan="2">Rata - Rata Waktu per RIT</td>
                </tr>
                <tr class="head">
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                </tr>

              <?php
              $nomor = 1;
              while ($data = mysql_fetch_array($query)) {
                $a1 = 0;
                $jumlahperrit = 0;
              ?>

                <tbody>
                  <tr>
                    <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['tanggal']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>
                    <td><?php echo $data['masuk1']; ?></td>
                    <td><?php echo $data['masuk2']; ?></td>
                    <td><?php echo $data['masuk3']; ?></td>
                    <td><?php echo $data['masuk4']; ?></td>
                    <td><?php echo $data['masuk5']; ?></td>
                    <?php 
                    // $total_selisih[] = [];
                      for($ich=1; $ich<=4;$ich++){
                        echo "<td>";
                        // $ich = 1;
                        $a1 += strlen($data["selisih$ich"]>0) ? 1 :0;
                        $jumlahperrit += $a1;

                        if ($data["selisih$ich"] > 0) {
                          echo $data["selisih$ich"]; 
                          $total_selisih[] = $data["selisih$ich"]; 
                        } else{
                          echo "00:00:00";
                        }
                        echo "</td>";
                      }
                    ?>

                    <td><?php echo $a1; ?></td>
                    <td><?php echo sum_time($total_selisih);?></td>
                    <td><?php $sum = sum_time($total_selisih); echo avg_time($sum, $a1);?></td>
                  </tr>
                </tbody>

                <?php
                  $h2 = [];
                  $kolomtotalselisihritasi[] = sum_time($total_selisih);
                  $totalkolomselisihritasi = sum_time2($kolomtotalselisihritasi);
                ?>
                <?php
                  $h3 = [];
                  $kolomratawaktuperit[] = avg_time($sum, $a1);
                  $totalkolomratawaktuperit = sum_time3($kolomratawaktuperit);
                ?>
              
              <?php } 
                    if (mysql_num_rows($query)==0) {
                      echo "<p>pencarian tidak ditemukan</p>";
                    }
                  ?>
                <tfoot class="head">
                    <tr>
                      <td colspan="12">Total</td>
                      <td><?php echo $jumlahperrit; ?></td>
                      <td><?php echo $totalkolomselisihritasi; ?></td>
                      <td><?php echo $totalkolomratawaktuperit; ?></td>
                    </tr>
                    <tr>
                      <td colspan="14">Total Rata - Rata</td>
                      <td><?php $tot = $totalkolomratawaktuperit/28; echo number_format($tot,2); ?></td>
                    </tr>
                  </tfoot>
            </table>
        </div>
        <br>
        <div class="col-md-12">
          <form method="post" action="perdagangan/perdagangan_waktumasuk.php" target="_blank">
            <div class="col-md-10">
              <div class="form-group" hidden="hidden">
                <label>No. Polisi</label>
                <input type="text" value="<?php echo $_POST['nopol'];?>" name="nopol" class="form-control">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
              </div>
            </div>
          </form>
        </div>

      <?php
        } else { unset($_POST['pencarian']); }  
      ?>           
    </div>
</div>

<?php include('template/footer.php');?>