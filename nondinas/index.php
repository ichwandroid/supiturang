<?php include('template/header.php');?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">                        
                        <div style="text-align:center">
							<p><img src="../assets/img/Logo_Kota_Malang_color.png" width="100" height="auto"></p>
							<h2>Aplikasi Pengelolaan Sampah</h2><h3>Dinas Kebersihan Kota Malang</h3>

							<p>Selamat datang di Halaman Admin, Anda Login dengan username <?php echo $_SESSION['nondinas']; ?></p>
						</div>
                        <div class="row">
                            <div class="col-md-3">&nbsp;</div>                        
                            <div class="col-md-3">
                                <a class="btn btn-success btn-block" href="nondinas_laporan.php">Laporan<br> Sampah Perhari</a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-success btn-block" href="nondinas_pembuangan.php">Pembuangan<br> Sampah Bulanan</a>
                            </div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	

<?php include('template/footer.php');?>