<?php include('template/header.php'); ?>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Form</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="nonDinas/input-action.php">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>No. Polisi</label>
                                                <input type="text" name="nopol" id="nopol" placeholder="Cari No. Polisi" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Jenis Kendaraan</label>
                                                <input type="text" name="jenis_kendaraan" id="jenis_kendaraan" placeholder="Jenis Kendaraan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Asal Kendaraan</label>
                                                <input type="text" name="asal" id="asal" class="form-control" placeholder="Asal Kendaraan">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ID Kendaraan</label> -->
                                                <input type="hidden" name="id_kendaraan" id="id_kendaraan" class="form-control" placeholder="ID Kendaraan">
                                            <!-- </div>
                                        </div> -->
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Waktu 1</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk1" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar1" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ritasi</label>
                                            <input type="text" name="vol1" value="" class="form-control">
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Waktu 2</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk2" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar2" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ritasi</label>
                                            <input type="text" name="vol2" value="" class="form-control">
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Waktu 3</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk3" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar3" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ritasi</label>
                                            <input type="text" name="vol3" value="" class="form-control">
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Volume</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nilai</label>
                                            <input type="text" name="volume" value="" class="form-control">
                                        </div>
                                    </div>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                        <button type="submit" name="simpan" value="Simpan" class="btn btn-success btn-fill">Simpan</button>
                   </form>
                </div>
            </div>
        </div>
        
        <!-- <script type="text/javascript">
            function autofill() {
                var nopol = $("#nopol").val();
                $.ajax({
                    url  : 'nonDinas/autofill-ajax.php',
                    data : 'nopol='+nopol,
                }).success(function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#jenis").val(obj.jenis);
                    $("#lambung").val(obj.lamb);
                    $("#sopir").val(obj.sopir);
                    $("#id_kendaraan").val(obj.id_kendaraan);
                });
            }
        </script> -->

<?php include('template/footer.php'); ?>
