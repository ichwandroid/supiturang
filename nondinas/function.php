<?php
function calc_time(array $total_selisih){
  $i = 0;
  foreach ($total_selisih as $time) {
    sscanf($time, '%d:%d:%d:', $hour, $min, $sec);
    $i += $hour * 3600 + $min * 60 + $sec;
  }
  return $i;
}

function sum_time(array $times) {
  $minutes = 0;
      // loop throught all the times
      foreach ($times as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }

      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;

      // returns the time already formatted
    return ( "$hours:$minutes:00");
} 

function avg_time($total, $count, $rounding = 0) {
  $total = explode(":", strval($total));
  if (count($total) !== 3) return false;
  $sum = $total[0]*60*60 + $total[1]*60 + $total[2];
  if ($sum == 0) {
    $average = 00;
  } else {
    $average = $sum/(float)$count;
  }
  $hours = floor($average/3600);
  $minutes = floor(fmod($average,3600)/60);
  $seconds = number_format(fmod(fmod($average,3600),60),(int)$rounding);
  return $hours.":".$minutes.":".$seconds;
} 
?>