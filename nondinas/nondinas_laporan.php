<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
      .tabel {
            width: 100%;
            text-align: center;
      }

      .tabel td {
            padding: 10px;
            border: solid 1px #000;
      }

      .head {
            background-color: #87CB16;
            height: 40px;
            border: solid 1px #000;
      }

      .head td {
            border: solid 1px #000;
      }

      .head1 td {
            width: 50%;
            padding: 10px;
            background-color: #87CB16;
            border: solid 1px #000;
      }
</style>

<div class="content">
	<div class="container-fluid">
		<div class="row col-md-12">
			<div class="header">
        <h4 class="title">Masukkan tanggal yang dicari</h4>
      </div>

      <!-- form -->
      <form method="post" action="nondinas_laporan.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d', strtotime('-31 days', strtotime(date('Y-m-d'))));?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?= date('Y-m-d');?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
      </form>

      <?php

        if (isset($_POST['pencarian'])) {
          
          $tanggalawal  = $_POST['tanggalawal'];
          $tanggalakhir = $_POST['tanggalakhir'];

          if (empty($tanggalawal)||empty($tanggalakhir)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'nondinas_laporan.php';
          </script>
          
          <?php
          } else {
          ?>

          <p>Informasi hasil pencarian tanggal <?php echo $_POST['tanggalawal'];?> sampai tanggal <?php echo $_POST['tanggalakhir'];?></p>
          
          <?php
            $query = mysql_query("SELECT in_out_nondinas.id, in_out_nondinas.tanggal, in_out_nondinas.jenis_kendaraan, in_out_nondinas.nopol, in_out_nondinas.masuk1, in_out_nondinas.keluar1, in_out_nondinas.vol1, in_out_nondinas.masuk2, in_out_nondinas.keluar2, in_out_nondinas.vol2, in_out_nondinas.masuk3, in_out_nondinas.keluar3, in_out_nondinas.vol3, in_out_nondinas.volume FROM in_out_nondinas WHERE tanggal BETWEEN '$tanggalawal' AND '$tanggalakhir'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width frame">
            <table class="tabel">
              <tr class="head">
                <td rowspan="2">No.</td>
                  <td rowspan="2">Tanggal</td>
                  <td rowspan="2">Jenis Kendaraan</td>
                  <td rowspan="2">No. Polisi</td>
                  <td colspan="3">Waktu 1</td>
                  <td colspan="3">Waktu 2</td>
                  <td colspan="3">Waktu 3</td>
                  <td colspan="2">Jumlah Volume</td>
                </tr>
                <tr class="head1">
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
                  <td>RIT</td>
                  <td>Volume</td>
              </tr>

              <?php
              $nomor = 1;
              while ($data = mysql_fetch_array($query)) {
                $a1 = 0;
              ?>

              <tbody>
                <tr>
                  <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['tanggal']; ?></td>
                    <td><?php echo $data['jenis_kendaraan']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>                   
                    <td><?php $a1 += strlen($data['masuk1']>0)?1:0; echo $data['masuk1']; ?></td>                   
                    <td><?php echo $data['keluar1']; ?></td>                   
                    <td><?php echo $data['vol1']; ?></td>                   
                    <td><?php $a1 += strlen($data['masuk2']>0)?1:0; echo $data['masuk2']; ?></td>                    
                    <td><?php echo $data['keluar2']; ?></td>                   
                    <td><?php echo $data['vol2']; ?></td>                    
                    <td><?php $a1 += strlen($data['masuk3']>0)?1:0; echo $data['masuk3']; ?></td>
                    <td><?php echo $data['keluar3']; ?></td>                   
                    <td><?php echo $data['vol3']; ?></td>
                    <td><?php echo $a1; ?></td>
                      <?php 
                       $m1 = $data['vol1']; 
                       $m2 = $data['vol2']; 
                       $m3 = $data['vol3']; 
                       $h = $m1+$m2+$m3;
                      ?>
                    <td><?php echo $h ;?></td>
                </tr>
              </tbody>
              
              <?php } 
                    if (mysql_num_rows($query)==0) {
                      echo "<p>pencarian tidak ditemukan</p>";
                    }
                  ?>
            </table>

    </div>
    <br>
    <div class="col-md-12">
    <form method="post" action="nondinas/nondinas_laporan.php" target="_blank">
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Awal</label>
            <input type="date" value="<?php echo $_POST['tanggalawal'];?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>&nbsp;</label>
            <button type="submit" name="pencarian" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
          </div>
        </div>
      </form>
    </div>
      <?php
        } else { unset($_POST['pencarian']); 
      }  
      ?>

  </div>
</div>

<?php include('template/footer.php');?>