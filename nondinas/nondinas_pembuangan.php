<?php include ('template/header.php');?>
<?php include ('function.php');?>

<style type="text/css">
      .tabel {
            width: 100%;
            text-align: center;
      }

      .tabel td {
            padding: 10px;
            border: solid 1px #000;
      }

      .head {
            background-color: #87CB16;
            height: 40px;
            border: solid 1px #000;
      }

      .head td {
            border: solid 1px #000;
      }

      .head1 td {
            width: 50%;
            padding: 10px;
            background-color: #87CB16;
            border: solid 1px #000;
      }
</style>

<div class="content">
  <div class="container-fluid">
    <div class="row col-md-12">
      <div class="header">
        <h4 class="title">Masukkan tanggal yang dicari</h4>
      </div>

      <!-- form -->
      <form method="post" action="nondinas_pembuangan.php">
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d', strtotime('-31 days', strtotime(date('Y-m-d'))));?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="date" value="<?= date('Y-m-d');?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>&nbsp;</label>
            <input type="submit" name="pencarian" value="Pencarian Data" class="btn btn-success btn-fill form-control">
          </div>
        </div>
      </form>

      <?php

        if (isset($_POST['pencarian'])) {
          
          $tanggalawal  = $_POST['tanggalawal'];
          $tanggalakhir = $_POST['tanggalakhir'];

          if (empty($tanggalawal)||empty($tanggalakhir)) {
            
          ?>

          <script type="text/javascript">
            alert ('Tanggal awal dan akhir harus di isi!');
            document.location = 'nondinas_pembuangan.php';
          </script>
          
          <?php
          } else {
          ?>

          <p>Informasi hasil pencarian tanggal <?php echo $_POST['tanggalawal'];?> sampai tanggal <?php echo $_POST['tanggalakhir'];?></p>

          <?php
            $query = mysql_query("SELECT in_out_nondinas.id, in_out_nondinas.tanggal, in_out_nondinas.jenis_kendaraan, in_out_nondinas.asal, in_out_nondinas.nopol, in_out_nondinas.masuk1, in_out_nondinas.vol1, in_out_nondinas.masuk2, in_out_nondinas.vol2, in_out_nondinas.masuk3, in_out_nondinas.vol3, in_out_nondinas.volume FROM in_out_nondinas WHERE tanggal BETWEEN '$tanggalawal' AND '$tanggalakhir'")or die(mysql_error());
          }
          ?>

          <div class="content table-responsive table-full-width frame">
            <table class="tabel">
              <tr class="head">
                <td>No.</td>
                <td>Jenis Kendaraan</td>
                <td>Asal Kendaraan</td>
                <td>No. Polisi</td>
                <td>Jumlah Ritasi</td>
                <td>Total Ritasi</td>
                <td>Volume Sampah</td>
                <td>Total Volume Sampah</td>
              </tr>

              <?php
              $a1 = 0;
              $nomor = 1;
              while ($data = mysql_fetch_array($query)) {
              ?>

              <tbody>
                <tr>
                  <td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['jenis_kendaraan']; ?></td>
                    <td><?php echo $data['asal']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>                   
                    <?php 
                      $a1 += strlen($data['masuk1']>0)?1:0;
                      $a1 += strlen($data['masuk2']>0)?1:0;
                      $a1 += strlen($data['masuk3']>0)?1:0;
                      $m1 = $data['vol1']; 
                      $m2 = $data['vol2']; 
                      $m3 = $data['vol3']; 
                      $h = $m1+$m2+$m3;
                    ?>
                    <td><?php echo $h ;?></td>
                    <td><?php echo $a1; ?></td>
                    <td><?php echo $data['volume']; ?></td>
                    <td><?php echo $data['volume']; ?></td> 
                </tr>
              </tbody>
              
              <?php } 
                    if (mysql_num_rows($query)==0) {
                      echo "<p>pencarian tidak ditemukan</p>";
                    }
                  ?>
            </table>


    </div>
    <br>
    <div class="col-md-12">
    <form method="post" action="nondinas/nondinas_pembuangan.php" target="_blank">
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Awal</label>
            <input type="date" value="<?php echo $_POST['tanggalawal'];?>" name="tanggalawal" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
            <input type="date" value="<?php echo $_POST['tanggalakhir'];?>" name="tanggalakhir" class="form-control">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group" hidden="hidden">
            <label>Tanggal Akhir</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>&nbsp;</label>
            <button type="submit" name="pencarian" class="btn btn-success form-control"><i class="fa fa-print" aria-hidden="true"></i>Cetak Laporan</button>
          </div>
        </div>
      </form>
    </div>
      <?php
        } else { unset($_POST['pencarian']); }  
      ?>
  </div>
</div>

<?php include('template/footer.php');?>