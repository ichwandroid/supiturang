<?php include ('template/header.php');?>

<style type="text/css">
      .tabel {
            width: 100%;
            text-align: center;
      }

      .tabel td {
            padding: 10px;
            border: solid 1px #000;
      }

      .head {
            background-color: #87CB16;
            height: 40px;
            border: solid 1px #000;
      }

      .head td {
            border: solid 1px #000;
      }

      .head1 td {
            /*width: 50%;*/
            padding: 10px;
            background-color: #87CB16;
            border: solid 1px #000;
      }
</style>

<div class="content">
	<div class="container-fluid">
		<div class="row col-md-12">
			<div class="header">
                        <?php 
                              if(isset($_GET['pesan'])){
                                    $pesan = $_GET['pesan'];
                                    if($pesan == "input"){
                                          echo "<div class='alert alert-success' role='alert'><i class='fa fa-check-square' aria-hidden='true'></i> Data berhasil di input.</div>";
                                    }else if($pesan == "update"){
                                          echo "<div class='alert alert-info' role='alert'><i class='fa fa-retweet' aria-hidden='true'></i> Data berhasil di update.</div>";
                                    }else if($pesan == "hapus"){
                                          echo "<div class='alert alert-warning' role='alert'><i class='fa fa-remove' aria-hidden='true'></i> Data berhasil di hapus.</div>";
                                    }
                              }
                        ?>
                        <h4 class="title">UPT TPA Supit Urang Kota Malang</h4>
                        <div class="col-md-6">
                          <p class="category"><a class="btn btn-success" href="nonDinas_input.php"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Data Baru</a></p>
                        </div>
                        <div class="col-md-6">
          <div class="form-group">
            <input id="myInput" onkeyup="myFunction()" class="form-control" type="text" placeholder="Pencarian..." title="No Polisi">
          </div>
        </div>
                  </div>
            <div class="content table-responsive table-full-width frame">
            	<table class="tabel" id="myTable">
            		<tr class="head">
                  <td rowspan="2">No.</td>
                  <td rowspan="2">Tanggal</td>
                  <td rowspan="2">Jenis Kendaraan</td>
                  <td rowspan="2">No. Polisi</td>
                  <td colspan="3">Waktu 1</td>
                  <td colspan="3">Waktu 2</td>
                  <td colspan="3">Waktu 3</td>
                  <td colspan="2">Jumlah Volume</td>
                  <td rowspan="2">Keterangan</td>
                </tr>
                <tr class="head1">
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
                  <td>Masuk</td>
                  <td>Keluar</td>
                  <td>Volume</td>
            			<td>RIT</td>
                  <td>Volume</td>
            		</tr>

            		<?php
            			$query_mysql = mysql_query("SELECT in_out_nondinas.id, in_out_nondinas.tanggal, in_out_nondinas.jenis_kendaraan, in_out_nondinas.nopol, in_out_nondinas.masuk1, in_out_nondinas.keluar1, in_out_nondinas.vol1, in_out_nondinas.masuk2, in_out_nondinas.keluar2, in_out_nondinas.vol2, in_out_nondinas.masuk3, in_out_nondinas.keluar3, in_out_nondinas.vol3, in_out_nondinas.volume FROM in_out_nondinas")or die(mysql_error());
            			$nomor = 1;
                  $a1 = 0;
            			while($data = mysql_fetch_array($query_mysql)){
            		?>

            		<tbody>
            			<tr>
            				<td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['tanggal']; ?></td>
                    <td><?php echo $data['jenis_kendaraan']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>                   
                    <td><?php $a1 += strlen($data['masuk1']>0)?1:0; echo $data['masuk1']; ?></td>                   
                    <td><?php echo $data['keluar1']; ?></td>                   
                    <td><?php echo $data['vol1']; ?></td>                   
                    <td><?php $a1 += strlen($data['masuk2']>0)?1:0; echo $data['masuk2']; ?></td>                    
                    <td><?php echo $data['keluar2']; ?></td>                   
                    <td><?php echo $data['vol2']; ?></td>                    
                    <td><?php $a1 += strlen($data['masuk3']>0)?1:0; echo $data['masuk3']; ?></td>
                    <td><?php echo $data['keluar3']; ?></td>                   
                    <td><?php echo $data['vol3']; ?></td>
                    <td><?php echo $a1; ?></td>
                    <td><?php echo $data['volume']; ?></td>
                    <td>
                      <a class="edit" href="nonDinas_edit.php?id=<?php echo $data['id']; ?>">Edit</a> | 
                      <a class="hapus" href="nonDinas/delete-action.php?id=<?php echo $data['id']; ?>">Hapus</a> 
                    </td>
                  </tr>
                </tbody>
                        <!-- modal -->
                           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="false">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Kendaraan No. Polisi : <?php echo $data['nopol']; ?> </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="fetched-data"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                  </table>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : 'nonDinas/detail-action.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
  </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="search.js"></script>
<?php include('template/footer.php');?>