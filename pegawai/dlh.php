<?php include ('template/header.php');?>
<link rel="stylesheet" type="text/css" href="template/style.css">

<div class="content">
	<div class="container-fluid">
		<div class="row col-md-12">
			<div class="header">
                        <?php 
                              if(isset($_GET['pesan'])){
                                    $pesan = $_GET['pesan'];
                                    if($pesan == "input"){
                                          echo "<div class='alert alert-success' role='alert'><i class='fa fa-check-square' aria-hidden='true'></i> Data berhasil di input.</div>";
                                    }else if($pesan == "update"){
                                          echo "<div class='alert alert-info' role='alert'><i class='fa fa-retweet' aria-hidden='true'></i> Data berhasil di update.</div>";
                                    }else if($pesan == "hapus"){
                                          echo "<div class='alert alert-warning' role='alert'><i class='fa fa-remove' aria-hidden='true'></i> Data berhasil di hapus.</div>";
                                    }
                              }
                        ?>
                        <h4 class="title">Dinas Kebersihan Kota Malang</h4>
        <div class="col-md-6">
          <p class="category"><a class="btn btn-success" href="dlh_input.php"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Data Baru</a></p>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <input id="myInput" onkeyup="myFunction()" class="form-control" type="text" placeholder="Pencarian..." title="No Polisi">
          </div>
        </div>
                  </div>
            <div class="content table-responsive table-full-width frame">
            	<table class="tabel" id="myTable">
            		<tr class="head">
                              <td rowspan="2">No.</td>
            			<td rowspan="2">Tanggal</td>
                              <td rowspan="2">Jenis Kendaraan</td>
                              <td rowspan="2">No. Polisi</td>
                              <td rowspan="2">No. Lambung</td>
                              <td rowspan="2">Nama Supir</td>
                              <td colspan="3">Waktu 1</td>
                              <td colspan="3">Waktu 2</td>
                              <td colspan="3">Waktu 3</td>
                              <td colspan="3">Waktu 4</td>
                              <td colspan="3">Waktu 5</td>
                              <td colspan="3">Waktu 6</td>
                              <td colspan="3">Waktu 7</td>
                              <td colspan="3">Jumlah Volume</td>
                              <td rowspan="2">Jumlah Ritasi</td>
                              <td rowspan="2">Keterangan</td>
                        </tr>
                        <tr class="head1">
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>Masuk</td>
            			<td>Keluar</td>
            			<td>Volume</td>
            			<td>A</td>
            			<td>B</td>
            			<td>C</td>
            		</tr>

            		<?php
            			$query_mysql = mysql_query("SELECT in_out_dlh.id, in_out_dlh.tanggal, kendaraan.jenis, kendaraan.nopol, kendaraan.lamb, kendaraan.sopir, in_out_dlh.masuk1, in_out_dlh.keluar1, in_out_dlh.vol1, in_out_dlh.vol2, in_out_dlh.vol3, in_out_dlh.vol4, in_out_dlh.vol5, in_out_dlh.vol6, in_out_dlh.vol7, timediff(in_out_dlh.keluar1,in_out_dlh.masuk1) AS 'selisih1', in_out_dlh.masuk2, in_out_dlh.keluar2, timediff(in_out_dlh.keluar2,in_out_dlh.masuk2) AS 'selisih2', in_out_dlh.masuk3, in_out_dlh.keluar3, timediff(in_out_dlh.keluar3,in_out_dlh.masuk3) AS 'selisih3', in_out_dlh.masuk4, in_out_dlh.keluar4, timediff(in_out_dlh.keluar4,in_out_dlh.masuk4) AS 'selisih4', in_out_dlh.masuk5, in_out_dlh.keluar5, timediff(in_out_dlh.keluar5,in_out_dlh.masuk5) AS 'selisih5', in_out_dlh.masuk6, in_out_dlh.keluar6, timediff(in_out_dlh.keluar6,in_out_dlh.masuk6) AS 'selisih6', in_out_dlh.masuk7, in_out_dlh.keluar7, timediff(in_out_dlh.keluar7,in_out_dlh.masuk7) AS 'selisih7', ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')) AS 'ritasiA', ((in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')) AS 'ritasiB', ((in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C')) AS 'ritasiC', ((in_out_dlh.vol1 LIKE 'A')+(in_out_dlh.vol2 LIKE 'A')+(in_out_dlh.vol3 LIKE 'A')+(in_out_dlh.vol4 LIKE 'A')+(in_out_dlh.vol5 LIKE 'A')+(in_out_dlh.vol6 LIKE 'A')+(in_out_dlh.vol7 LIKE 'A')+(in_out_dlh.vol1 LIKE 'B')+(in_out_dlh.vol2 LIKE 'B')+(in_out_dlh.vol3 LIKE 'B')+(in_out_dlh.vol4 LIKE 'B')+(in_out_dlh.vol5 LIKE 'B')+(in_out_dlh.vol6 LIKE 'B')+(in_out_dlh.vol7 LIKE 'B')+(in_out_dlh.vol1 LIKE 'C')+(in_out_dlh.vol2 LIKE 'C')+(in_out_dlh.vol3 LIKE 'C')+(in_out_dlh.vol4 LIKE 'C')+(in_out_dlh.vol5 LIKE 'C')+(in_out_dlh.vol6 LIKE 'C')+(in_out_dlh.vol7 LIKE 'C')) AS 'total_ritasi' FROM in_out_dlh INNER JOIN kendaraan ON in_out_dlh.id_kendaraan = kendaraan.id_kendaraan")or die(mysql_error());
            			$nomor = 1;
            			while($data = mysql_fetch_array($query_mysql)){
            		?>

            		<tbody>
            			<tr>
            				<td><?php echo $nomor++; ?></td>
                    <td><?php echo $data['tanggal']; ?></td>
                    <td><?php echo $data['jenis']; ?></td>
                    <td><?php echo $data['nopol']; ?></td>
                    <td><?php echo $data['lamb']; ?></td>
                    <td><?php echo $data['sopir']; ?></td>
                    <td><?php echo $data['masuk1']; ?></td>
                    <td><?php echo $data['keluar1']; ?></td>
                    <td><?php echo $data['vol1']; ?></td>
                    <td><?php echo $data['masuk2']; ?></td>
                    <td><?php echo $data['keluar2']; ?></td>
                    <td><?php echo $data['vol2']; ?></td>
                    <td><?php echo $data['masuk3']; ?></td>
                    <td><?php echo $data['keluar3']; ?></td>
                    <td><?php echo $data['vol3']; ?></td>
                    <td><?php echo $data['masuk4']; ?></td>
                    <td><?php echo $data['keluar4']; ?></td>
                    <td><?php echo $data['vol4']; ?></td>
                    <td><?php echo $data['masuk5']; ?></td>
                    <td><?php echo $data['keluar5']; ?></td>
                    <td><?php echo $data['vol5']; ?></td>
                    <td><?php echo $data['masuk6']; ?></td>
                    <td><?php echo $data['keluar6']; ?></td>
                    <td><?php echo $data['vol6']; ?></td>
                    <td><?php echo $data['masuk7']; ?></td>
                    <td><?php echo $data['keluar7']; ?></td>
                    <td><?php echo $data['vol7']; ?></td>
                    <td><?php echo $data['ritasiA']; ?></td>
                    <td><?php echo $data['ritasiB']; ?></td>
                    <td><?php echo $data['ritasiC']; ?></td>
                    <td><?php echo $data['total_ritasi']; ?></td>
                    <td>
                      <a class="edit" href="dlh_edit.php?id=<?php echo $data['id']; ?>">Edit</a> | 
                      <a class="hapus" href="dlh/delete-action.php?id=<?php echo $data['id']; ?>">Hapus</a> 
                      <?php echo "<a href='#myModal' class='btn btn-success' id='custId' data-toggle='modal' data-id=".$data['id'].">Detail</a>"; ?>
                    </td>
                  </tr>
                </tbody>
                        <!-- modal -->
                           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="false">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Kendaraan No. Polisi : <?php echo $data['nopol']; ?> </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="fetched-data"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                  </table>
            </div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : 'dlh/detail-action.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
  </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        </div>
    </div>
</div>


<script type="text/javascript" src="search.js"></script>
<?php include('template/footer.php');?>