<?php include('template/header.php'); ?>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Form</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="dkp/input-action.php">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>No. Polisi</label>
                                                <input type="text" name="nopol" id="nopol" placeholder="Cari No. Polisi" class="form-control" onkeyup="autofill()">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Jenis Kendaraan</label>
                                                <input type="text" name="jenis" id="jenis" placeholder="Jenis Kendaraan" class="form-control" disabled="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Lambung</label>
                                                <input type="text" name="lambung" id="lambung" value="" class="form-control" placeholder="Lambung" disabled="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Pengemudi</label>
                                                <input type="text" name="sopir" id="sopir" class="form-control" placeholder="pengemudi" disabled="">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ID Kendaraan</label> -->
                                                <input type="hidden" name="id_kendaraan" id="id_kendaraan" class="form-control" placeholder="ID Kendaraan">
                                            <!-- </div>
                                        </div> -->
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Waktu 1</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk1" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar1" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol1">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 2</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk2" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar2" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol2">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 3</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk3" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar3" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol3">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 4</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk4" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar4" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol4">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 5</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk5" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar5" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol5">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 6</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk6" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar6" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol6">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <h4 class="title">Waktu 7</h4>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input type="time" name="masuk7" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input type="time" name="keluar7" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jenis Volume</label>
                                            <select class="form-control" name="vol7">
                                                <option value="">Pilih Volume</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <button type="submit" name="simpan" value="Simpan" class="btn btn-success btn-fill">Simpan</button>
                            </div>
                                </form>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            function autofill() {
                var nopol = $("#nopol").val();
                $.ajax({
                    url  : 'dkp/autofill-ajax.php',
                    data : 'nopol='+nopol,
                }).success(function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#jenis").val(obj.jenis);
                    $("#lambung").val(obj.lamb);
                    $("#sopir").val(obj.sopir);
                    $("#id_kendaraan").val(obj.id_kendaraan);
                });
            }
        </script>

<?php include('template/footer.php'); ?>
