<?php include ('template/header.php');?>
<link rel="stylesheet" type="text/css" href="template/style.css">

<div class="content">
	<div class="container-fluid">
		<div class="row col-md-12">
			<div class="header">
                <?php if(isset($_GET['pesan'])){
                    $pesan = $_GET['pesan'];
                    if($pesan == "input"){
                        echo "<div class='alert alert-success' role='alert'><i class='fa fa-check-square' aria-hidden='true'></i> Data berhasil di input.</div>";
                    }else if($pesan == "update"){
                        echo "<div class='alert alert-info' role='alert'><i class='fa fa-retweet' aria-hidden='true'></i> Data berhasil di update.</div>";
                    }else if($pesan == "hapus"){
                        echo "<div class='alert alert-warning' role='alert'><i class='fa fa-remove' aria-hidden='true'></i> Data berhasil di hapus.</div>";
                    }
                }
                ?>
                <h4 class="title">Kendaraan Dinas Kebersihan Kota Malang</h4>
                <p class="category"><a class="btn btn-success" href="kendaraan_input.php"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Data Baru</a></p>
            </div>
            <div class="content table-responsive table-full-width">
            	<table class="tabel">
            		<tr class="head">
                        <td>No.</td>
                        <td>Jenis Kendaraan</td>
                        <td>No. Polisi</td>
                        <td>No. Lambung</td>
                        <td>Nama Supir</td>
                        <td>Keterangan</td>
                    </tr>

            		<?php
            			$query_mysql = mysql_query("SELECT id_kendaraan, jenis, nopol, lamb, sopir FROM kendaraan")or die(mysql_error());
            			$nomor = 1;
            			while($data = mysql_fetch_array($query_mysql)){
            		?>

            		<tbody>
            			<tr>
            				<td><?php echo $nomor++; ?></td>
                            <td><?php echo $data['jenis']; ?></td>
                            <td><?php echo $data['nopol']; ?></td>
                            <td><?php echo $data['lamb']; ?></td>
                            <td><?php echo $data['sopir']; ?></td>
                            <td>
                                <a class="edit" href="kendaraan_edit.php?id=<?php echo $data['id_kendaraan']; ?>">Edit</a> | 
                                <a class="hapus" href="kendaraan/delete-action.php?id=<?php echo $data['id_kendaraan']; ?>">Hapus</a>
                            </td>
                        </tr>
                    </tbody>            

                        <?php } ?>

                  </table>
            </div>

        </div>
    </div>
</div>

<?php include('template/footer.php');?>