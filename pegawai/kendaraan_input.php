<?php include('template/header.php'); ?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Form</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="Kendaraan/input-action.php">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>No. Polisi</label>
                                                <input type="text" name="nopol" id="nopol" placeholder="Cari No. Polisi" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Jenis Kendaraan</label>
                                                <input type="text" name="jenis" id="jenis" placeholder="Jenis Kendaraan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Lambung</label>
                                                <input type="text" name="lamb" id="lamb" value="" class="form-control" placeholder="Lambung">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Pengemudi</label>
                                                <input type="text" name="sopir" id="sopir" class="form-control" placeholder="pengemudi">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" name="simpan" value="Simpan" class="btn btn-success btn-fill">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include('template/footer.php'); ?>
