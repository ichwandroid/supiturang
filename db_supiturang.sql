-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Agu 2017 pada 00.09
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_supiturang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_out_dkp`
--

CREATE TABLE `in_out_dkp` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_kendaraan` int(5) NOT NULL,
  `masuk1` time NOT NULL,
  `keluar1` time NOT NULL,
  `vol1` text NOT NULL,
  `masuk2` time NOT NULL,
  `keluar2` time NOT NULL,
  `vol2` text NOT NULL,
  `masuk3` time NOT NULL,
  `keluar3` time NOT NULL,
  `vol3` text NOT NULL,
  `masuk4` time NOT NULL,
  `keluar4` time NOT NULL,
  `vol4` text NOT NULL,
  `masuk5` time NOT NULL,
  `keluar5` time NOT NULL,
  `vol5` text NOT NULL,
  `masuk6` time NOT NULL,
  `keluar6` time NOT NULL,
  `vol6` text NOT NULL,
  `masuk7` time NOT NULL,
  `keluar7` time NOT NULL,
  `vol7` text NOT NULL,
  `ritasiA` int(11) NOT NULL,
  `ritasiB` int(11) NOT NULL,
  `ritasiC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_out_dkp`
--

INSERT INTO `in_out_dkp` (`id`, `tanggal`, `id_kendaraan`, `masuk1`, `keluar1`, `vol1`, `masuk2`, `keluar2`, `vol2`, `masuk3`, `keluar3`, `vol3`, `masuk4`, `keluar4`, `vol4`, `masuk5`, `keluar5`, `vol5`, `masuk6`, `keluar6`, `vol6`, `masuk7`, `keluar7`, `vol7`, `ritasiA`, `ritasiB`, `ritasiC`) VALUES
(1, '2017-08-10', 1, '06:50:00', '07:00:00', 'A', '08:25:00', '08:45:00', 'A', '10:10:00', '10:15:00', 'A', '11:30:00', '12:00:00', 'B', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0),
(2, '2017-08-10', 2, '09:00:00', '09:15:00', 'B', '11:40:00', '11:55:00', 'B', '13:15:00', '13:45:00', 'C', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0),
(3, '2017-08-10', 3, '07:15:00', '07:35:00', 'C', '09:40:00', '10:00:00', 'A', '11:35:00', '12:00:00', 'B', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0),
(4, '2017-08-23', 1, '10:00:00', '10:15:00', 'A', '11:00:00', '11:20:00', 'B', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_out_dlh`
--

CREATE TABLE `in_out_dlh` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_kendaraan` int(5) NOT NULL,
  `masuk1` time NOT NULL,
  `keluar1` time NOT NULL,
  `vol1` text NOT NULL,
  `masuk2` time NOT NULL,
  `keluar2` time NOT NULL,
  `vol2` text NOT NULL,
  `masuk3` time NOT NULL,
  `keluar3` time NOT NULL,
  `vol3` text NOT NULL,
  `masuk4` time NOT NULL,
  `keluar4` time NOT NULL,
  `vol4` text NOT NULL,
  `masuk5` time NOT NULL,
  `keluar5` time NOT NULL,
  `vol5` text NOT NULL,
  `masuk6` time NOT NULL,
  `keluar6` time NOT NULL,
  `vol6` text NOT NULL,
  `masuk7` time NOT NULL,
  `keluar7` time NOT NULL,
  `vol7` text NOT NULL,
  `ritasiA` int(11) NOT NULL,
  `ritasiB` int(11) NOT NULL,
  `ritasiC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_out_dlh`
--

INSERT INTO `in_out_dlh` (`id`, `tanggal`, `id_kendaraan`, `masuk1`, `keluar1`, `vol1`, `masuk2`, `keluar2`, `vol2`, `masuk3`, `keluar3`, `vol3`, `masuk4`, `keluar4`, `vol4`, `masuk5`, `keluar5`, `vol5`, `masuk6`, `keluar6`, `vol6`, `masuk7`, `keluar7`, `vol7`, `ritasiA`, `ritasiB`, `ritasiC`) VALUES
(1, '2017-08-20', 8, '13:00:00', '13:15:00', 'A', '14:00:00', '14:30:00', 'A', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0),
(2, '2017-08-20', 1, '15:00:00', '15:15:00', 'A', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', '00:00:00', '00:00:00', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_out_nondinas`
--

CREATE TABLE `in_out_nondinas` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis_kendaraan` text NOT NULL,
  `asal` text NOT NULL,
  `Nopol` text NOT NULL,
  `masuk1` time NOT NULL,
  `keluar1` time NOT NULL,
  `vol1` int(11) NOT NULL,
  `masuk2` time NOT NULL,
  `keluar2` time NOT NULL,
  `vol2` int(11) NOT NULL,
  `masuk3` time NOT NULL,
  `keluar3` time NOT NULL,
  `vol3` int(11) NOT NULL,
  `volume` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_out_nondinas`
--

INSERT INTO `in_out_nondinas` (`id`, `tanggal`, `jenis_kendaraan`, `asal`, `Nopol`, `masuk1`, `keluar1`, `vol1`, `masuk2`, `keluar2`, `vol2`, `masuk3`, `keluar3`, `vol3`, `volume`) VALUES
(1, '2017-08-27', 'CT', 'Malang', 'N2018AA', '14:00:00', '14:30:00', 3, '14:45:00', '15:00:00', 4, '00:00:00', '00:00:00', 0, 9),
(2, '2017-08-24', 'AA', 'Malang', 'N2017PP', '12:00:00', '00:00:00', 4, '12:30:00', '00:00:00', 1, '00:00:00', '00:00:00', 0, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_out_pasar`
--

CREATE TABLE `in_out_pasar` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_kendaraan` int(5) NOT NULL,
  `masuk1` time NOT NULL,
  `keluar1` time NOT NULL,
  `vol1` text NOT NULL,
  `masuk2` time NOT NULL,
  `keluar2` time NOT NULL,
  `vol2` text NOT NULL,
  `masuk3` time NOT NULL,
  `keluar3` time NOT NULL,
  `vol3` text NOT NULL,
  `masuk4` time NOT NULL,
  `keluar4` time NOT NULL,
  `vol4` text NOT NULL,
  `masuk5` time NOT NULL,
  `keluar5` time NOT NULL,
  `vol5` text NOT NULL,
  `masuk6` time NOT NULL,
  `keluar6` time NOT NULL,
  `vol6` text NOT NULL,
  `masuk7` time NOT NULL,
  `keluar7` time NOT NULL,
  `vol7` text NOT NULL,
  `ritasiA` int(11) NOT NULL,
  `ritasiB` int(11) NOT NULL,
  `ritasiC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_out_perdagangan`
--

CREATE TABLE `in_out_perdagangan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_kendaraan` int(5) NOT NULL,
  `masuk1` time NOT NULL,
  `keluar1` time NOT NULL,
  `vol1` text NOT NULL,
  `masuk2` time NOT NULL,
  `keluar2` time NOT NULL,
  `vol2` text NOT NULL,
  `masuk3` time NOT NULL,
  `keluar3` time NOT NULL,
  `vol3` text NOT NULL,
  `masuk4` time NOT NULL,
  `keluar4` time NOT NULL,
  `vol4` text NOT NULL,
  `masuk5` time NOT NULL,
  `keluar5` time NOT NULL,
  `vol5` text NOT NULL,
  `masuk6` time NOT NULL,
  `keluar6` time NOT NULL,
  `vol6` text NOT NULL,
  `masuk7` time NOT NULL,
  `keluar7` time NOT NULL,
  `vol7` text NOT NULL,
  `ritasiA` int(11) NOT NULL,
  `ritasiB` int(11) NOT NULL,
  `ritasiC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` int(11) NOT NULL,
  `jenis` varchar(5) NOT NULL,
  `nopol` varchar(10) NOT NULL,
  `lamb` varchar(10) NOT NULL,
  `sopir` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kendaraan`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `jenis`, `nopol`, `lamb`, `sopir`) VALUES
(1, 'CT', 'N8196AP', '1', 'A'),
(2, 'CT', 'N8011AP', '2', 'B'),
(3, 'CT', 'N8012AP', '3', 'C'),
(4, 'CT', 'N8017AP', '4', 'D'),
(5, 'CT', 'N8016AP', '5', 'E'),
(6, 'DT', 'L8096YX', '6', 'F'),
(7, 'DT', 'N8090AP', '7', 'G'),
(8, 'A', 'h7897', 's', 'so'),
(13, 'CT', 'N2013AP', '1', 'Rizky');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1),
(2, 'pegawai', '047aeeb234644b9e2d4138ed3bc7976a', 'pegawai', 2),
(3, 'nondinas', '12535542aa9280de2b9cb142e0886ead', 'nondinas', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `in_out_dkp`
--
ALTER TABLE `in_out_dkp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `in_out_dlh`
--
ALTER TABLE `in_out_dlh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `in_out_nondinas`
--
ALTER TABLE `in_out_nondinas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `in_out_pasar`
--
ALTER TABLE `in_out_pasar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `in_out_perdagangan`
--
ALTER TABLE `in_out_perdagangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `in_out_dkp`
--
ALTER TABLE `in_out_dkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `in_out_dlh`
--
ALTER TABLE `in_out_dlh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `in_out_nondinas`
--
ALTER TABLE `in_out_nondinas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `in_out_pasar`
--
ALTER TABLE `in_out_pasar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `in_out_perdagangan`
--
ALTER TABLE `in_out_perdagangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
